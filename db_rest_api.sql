/*
 Navicat Premium Data Transfer

 Source Server         : DO - MAIN
 Source Server Type    : MySQL
 Source Server Version : 50729
 Source Host           : localhost:3306
 Source Schema         : db_rest_api

 Target Server Type    : MySQL
 Target Server Version : 50729
 File Encoding         : 65001

 Date: 11/03/2020 12:58:42
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for keys
-- ----------------------------
DROP TABLE IF EXISTS `keys`;
CREATE TABLE `keys` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `key` varchar(40) NOT NULL,
  `level` int(2) NOT NULL,
  `ignore_limits` tinyint(1) NOT NULL DEFAULT '0',
  `is_private_key` tinyint(1) NOT NULL DEFAULT '0',
  `ip_addresses` text,
  `date_created` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of keys
-- ----------------------------
BEGIN;
INSERT INTO `keys` VALUES (1, 1, 'test_v2', 1, 0, 0, NULL, 2008);
COMMIT;

-- ----------------------------
-- Table structure for produk
-- ----------------------------
DROP TABLE IF EXISTS `produk`;
CREATE TABLE `produk` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `uuid` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `price` int(10) DEFAULT NULL,
  `quantity` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of produk
-- ----------------------------
BEGIN;
INSERT INTO `produk` VALUES (1, 1, 'Jamu', 1000, '10');
INSERT INTO `produk` VALUES (2, 1, 'Apel', 1000, '10');
INSERT INTO `produk` VALUES (3, 1, 'Nanas', 1000, '10');
INSERT INTO `produk` VALUES (4, 2, 'Nanas', 11000, '10');
INSERT INTO `produk` VALUES (5, 2, 'Jeruk', 1000, '10');
INSERT INTO `produk` VALUES (6, 3, 'Nanas', 10000, '5');
INSERT INTO `produk` VALUES (7, 3, 'Labu', 2500, '10');
INSERT INTO `produk` VALUES (8, 3, 'SIngkong', 1000, '11');
COMMIT;

-- ----------------------------
-- Table structure for users
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `uuid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `fullname` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `phone` varchar(255) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`uuid`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of users
-- ----------------------------
BEGIN;
INSERT INTO `users` VALUES (1, 'Aditya Nurhadi', 'aditya@email.com', '123456', '000', '123123');
INSERT INTO `users` VALUES (2, 'christina dn', 'cdn@emaill.com', '123456', '000', '00000');
INSERT INTO `users` VALUES (3, 'kinandari', 'kdr@email.com', '123456', '000', '00000');
INSERT INTO `users` VALUES (4, 'dashk', 'demaxil@email.com', 'password', '', '');
INSERT INTO `users` VALUES (9, 'mike', 'yemail@email.com', 'password', '', '');
INSERT INTO `users` VALUES (10, 'mike', 'zemail@email.com', 'password', '', '');
INSERT INTO `users` VALUES (11, 'mike', 'demail@email.com', 'password', '', '');
INSERT INTO `users` VALUES (14, 'mike', 'xasd@email.com', 'password', '', '');
INSERT INTO `users` VALUES (15, 'mike', 'sss@email.com', 'password', '', '');
COMMIT;

SET FOREIGN_KEY_CHECKS = 1;
