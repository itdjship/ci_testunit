<?php
class Users_test extends TestCase
{

    public function setUp()
    {
        $this->resetInstance();
        $this->CI->load->model('mtest');
        $this->obj = $this->CI->mtest;
    }

    public function testGet()
    {
       # $this->request->enableHooks();
        $this->request->setHeader('api_key', 'test_v2');
        try {
            $output = $this->request('POST', 'users/check_uniq_email',array());
        } catch (CIPHPUnitTestExitException $e) {
            $output = ob_get_clean();
        }

        $this->assertEquals('{"status":true}', $output);
    }

    
    public function test_users_search()
	{
        $this->request->setHeader('api_key', 'test_v2');
        try {
            $output = $this->request('POST', 'users/search');
		} catch (CIPHPUnitTestExitException $e) {
			$output = ob_get_clean();
        }
		
        $expected = <<< 'EOL'
        {"status":true,"data":[{"uuid":"1","fullname":"Aditya Nurhadi","email":"aditya@email.com","phone":"000","address":"123123","produk_name":"Jamu,Apel,Nanas","produk":[{"name":"Jamu","price":"1000","quantity":"10"},{"name":"Apel","price":"1000","quantity":"10"},{"name":"Nanas","price":"1000","quantity":"10"}]},{"uuid":"2","fullname":"christina dn","email":"cdn@emaill.com","phone":"000","address":"00000","produk_name":"Nanas,Jeruk","produk":[{"name":"Nanas","price":"11000","quantity":"10"},{"name":"Jeruk","price":"1000","quantity":"10"}]}]}
        EOL;
        $response = json_decode($output);
        if($response->status){
            $this->assertEquals(true, $response->status);
            $this->assertTrue($response->status);
            $this->assertResponseCode(200);
        }else{
            $this->assertFalse($response->status);
            $this->assertResponseCode(400);
        }
        
        
        
        
    }
    
    
    
	public function test_store_new_user()
	{
        $this->request->setHeader('api_key', 'test_v2');
        try {
			$output = $this->request( 'POST', 'users/store', array('fullname' => 'mike','password'=>'password', 'email'=>'xzcasd@email.com','phone'=>'','address'=>''));
		} catch (CIPHPUnitTestExitException $e) {
			$output = ob_get_clean();
        }
        
        #$expected = <<< 'EOL'
        #{"status":true,"messsage":"Success"}
        #EOL;
        #$this->assertEquals($expected, $output);
        $response = json_decode($output);
        if($response->status){
            $this->assertEquals(true, $response->status);
            $this->assertTrue($response->status);
            $this->assertResponseCode(200);
        }else{
            $this->assertFalse($response->status);
            $this->assertResponseCode(200);
        }
    }
    
    
    public function test_store_update_user()
	{
        $this->request->setHeader('api_key', 'test_v2');
        try {
			$output = $this->request( 'POST', 'users/store', array('uuid'=>16, 'fullname' => 'dashk','password'=>'password', 'email'=>'demaxil@email.com','phone'=>'','address'=>''));
		} catch (CIPHPUnitTestExitException $e) {
			$output = ob_get_clean();
        }
        
        #$expected = <<< 'EOL'
        #{"status":true,"messsage":"Success"}
        #EOL;
        #$this->assertEquals($expected, $output);
        $response = json_decode($output);
        if($response->status){
            $this->assertEquals(true, $response->status);
            $this->assertTrue($response->status);
            $this->assertResponseCode(200);
        }else{
            $this->assertFalse($response->status);
            $this->assertResponseCode(200);
        }
    }

    public function test_users_delete()
    {
        $this->request->setHeader('api_key', 'test_v2');
        try {
            $output = $this->request('POST', 'users/delete',array('uuid'=>16));
		} catch (CIPHPUnitTestExitException $e) {
			$output = ob_get_clean();
        }
        #$expected = <<< 'EOL'
        #{"status":true}
        #EOL;
        #$this->assertEquals($expected, $output);
        $response = json_decode($output);
        if($response->status){
            $this->assertEquals(true, $response->status);
            $this->assertTrue($response->status);
            $this->assertResponseCode(200);
        }else{
            $this->assertFalse($response->status);
            $this->assertResponseCode(200);
        }
    }
}
