<?php defined('BASEPATH') OR exit('No direct script access allowed');

use chriskacerguis\RestServer\RestController;

class Users extends RestController {
    
	function __construct(){
        parent::__construct();
		$this->load->model('mtest');
    }
    
    public function check_uniq_email_post(){
        $result = array('status'=>true);

        echo json_encode($result);
        //$this->response($result, 200);
        //return $result;
    }
	
	public function store_post()
	{
        $this->load->library('form_validation');

        $fullname   = $this->post('fullname');
        $email      = $this->post('email');
        $password   = $this->post('password');
        $phone      = $this->post('phone');
        $address    = $this->post('address');
        $uuid       = $this->post('uuid') ? $this->post('uuid') : '';


        $this->form_validation->set_rules('fullname', 'Fullname', 'required');
        $this->form_validation->set_rules('password', 'Password', 'required');
        if($uuid == ''){
            $this->form_validation->set_rules('email', 'Email', 'required|valid_email|is_unique[users.email]');
        }else{
            $this->form_validation->set_rules('email', 'Email', array('required','valid_email'));
        }
        

        if($this->form_validation->run() == FALSE){
            
            $result = array('status'=>false, 'message'=>"Error : \n".validation_errors(null,null));

            //$this->response($result, 400);

        }else{
            
            $data       = array('fullname'=>$fullname,
                                'email'=>$email,
                                'password'=>$password,
                                'phone'=>$phone,
                                'address'=>$address);
            $query  = $this->mtest->stored($data, $uuid);
            if($query > 0){
                $result = array('status'=>true, 'messsage'=>'Success');
            }else{
                $result = array('status'=>false, 'message'=>'Error save');
            }

            //$this->response($result, 200);
            
        }   
        echo json_encode($result);     
    }

    public function delete_post(){
        $query = $this->mtest->delete($this->post('uuid'));
        
        if($query > 0){
            $result = array('status'=>true);
            //$this->response($result, 200);
        }else{
            $result = array('status'=>false);
            //$this->response($result, 400);
        }
        echo json_encode($result);
    }
	
    public function search_post()
    {
        $key        = $this->post('key');
        $cols       = $this->post('cols');
        $sort       = $this->post('sort');
        $limit      = $this->post('limit') ? $this->post('limit') : 2;
        $page       = $this->post('page') ? $this->post('page') : 0;
        $page       = $page > 0 ? $limit * $page : 0;

        $query     = $this->mtest->search($key, $cols, $sort, $limit, $page);
        
        $data       = array();
        $product    = array();
        if($query->num_rows() > 0){
            foreach($query->result() as $row){
                $row->produk = array();
                array_push($data, $row);
                $squery     = $this->mtest->getProduk($row->uuid, $key);
                if($squery->num_rows() > 0){
                    foreach($squery->result() as $srow){
                        array_push($row->produk, $srow);
                    }
                }
            }
		}
        $result     = array('status'=>true,'data'=>$data);
        echo json_encode($result);
        //$this->response($result, RESTController::HTTP_OK);
    }
    
}