<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 
class Mtest extends CI_Model{
	
    private $user	= 'users';
    private $produk = 'produk';
	
    
    public function stored($data, $uuid = ''){
        if($uuid != ''){
            $this->db->where('uuid', $uuid);
            $this->db->update($this->user, $data);
        }else{
            $this->db->insert($this->user, $data);
        }
        return $this->db->affected_rows();
    }

    public function delete($uuid){
        $this->db->where('uuid', $uuid);
        $this->db->delete($this->user);
        return $this->db->affected_rows();
    }

    public function search($key = '', $order = 'fullname', $sort = 'asc', $limit = '2', $page = 0)
    {
        $this->db->select('a.uuid, a.fullname, a.email, a.phone, a.address, group_concat(b.name) as produk_name', false);
        $this->db->join($this->produk.' b','a.uuid = b.uuid','left');
        (($key != '') ? $this->db->where('(a.fullname like "%'.$key.'%" or b.name like "%'.$key.'%")', null, false) : '');
        $this->db->order_by($order, $sort);
        $this->db->group_by('a.uuid');
        $this->db->limit($limit, $page);
        return $this->db->get($this->user.' a');
    }

    public function getProduk($uuid, $key = '')
    {
        $this->db->select('a.name, a.price, a.quantity', false);
        $this->db->where('a.uuid', $uuid);
        (($key != '') ? $this->db->where('(a.name like "%'.$key.'%")', null, false) : '');
        return $this->db->get($this->produk.' a ');
    }

    public function check_uniq_email($str){
        $this->db->select('count(*) as t', false);
        $this->db->where('email', $str);
        $query = $this->db->get($this->user);
        $row    = $query->row();
        return $row->t;
    }
}